from rest_framework.authtoken.models import Token
from rest_framework.authentication import TokenAuthentication
from rest_framework.response import Response
from rest_framework import status
from .models import *
from rest_framework.views import APIView
from rest_framework.exceptions import AuthenticationFailed
from django.db import IntegrityError


import json
class SignupUser(APIView):
    #================================= For User SignUp ====================================================#
    def post(self,request,*args,**kwargs):
        data = json.loads(request.body)
        try:
            user = User.objects.get(mobile=data["mobile"])
            token,created = Token.objects.get_or_create(user=user)

            return Response({
                'Status':'User Already Exist',
                'mobile':user.mobile,
                'username':user.username,
                'token':token.key
            })
        except User.DoesNotExist:
            user = User()
            user.username = data["username"]
            user.mobile = data["mobile"]
            user.name = data["name"]
            user.email = data["email"]

            user.set_password(data["password"])
            try:
                user.save()
            except IntegrityError:
                return Response({
                    "Status":"Username Or Email Is Already Taken"
                })
            token = Token.objects.create(user=user)

        return Response({
            'token': token.key
        })



class Login(APIView):
    #============================================== For User Login ==============================================#
    def post(self,request,*args,**kwargs):
        data = json.loads(request.body)
        mobile = data['mobile']
        password = data['password']
        try:
            user = User.objects.get(mobile=mobile,password=password)
        except User.DoesNotExist:
            return Response({
                'Status': "Invalid Credentials"
            })
        try:
            token = Token.objects.get(user=user)

        except Token.DoesNotExist:

            token = Token.objects.create(user=user)
        return Response({
            'token':token.key,

        },status=status.HTTP_200_OK)

class MarkSpam(APIView):

    #================================= ADD User to Exisitng Spam Number or Created a Span Number ===================#
    def post(self,request):
        try:
            user, token = TokenAuthentication().authenticate(request)
        except AuthenticationFailed as e:
            return Response({"message": "Invalid auth token"}, status=status.HTTP_401_UNAUTHORIZED)

        data = json.loads(request.body)
        try:
            s = SpammedNumbers.objects.get(mobile=data['mobile'])
        except SpammedNumbers.DoesNotExist:
            s = SpammedNumbers()
            s.mobile = data["mobile"]
            s.spammed_name = data["name"]
            s.save()
        s.spammed_by.add(user)
        return Response({
            'Status':"Successfully Spammed the Number {}".format(data['mobile']),
            # 'Numbers Spammed By User':user.get_all_spammed_contact()
        })

class AddPersonalContacts(APIView):
    #======================================= Get All Personal Contacts of User ======================================#
    def get(self,request):
        try:
            user, token = TokenAuthentication().authenticate(request)
        except AuthenticationFailed as e:
            return Response({"message": "Invalid auth token"}, status=status.HTTP_401_UNAUTHORIZED)

        data = []
        personal_contacts = PersonalContact.objects.filter(user=user)

        for numbers in personal_contacts:
            data.append({
                "mobile":numbers.mobile,
                "name":numbers.name,
            })
        return Response(data)
    #====================================== Add Personal Contacts to User ======================================#
    def post(self,request):
        try:
            user, token = TokenAuthentication().authenticate(request)
        except AuthenticationFailed as e:
            return Response({"message": "Invalid auth token"}, status=status.HTTP_401_UNAUTHORIZED)

        data = json.loads(request.body)
        try:
            p = PersonalContact.objects.get(mobile=data['mobile'],user=user)
            return Response({
                "Status":"Contact Number {} Already Present in {} Personal Contacts".format(p.mobile,user.mobile)
            })
        except PersonalContact.DoesNotExist:
            p  = PersonalContact.objects.create(mobile=data["mobile"],user=user,name=data["name"])
        return Response({
            'Status':"Successfully Added Personal Contact to the User {}".format(user),
        })

class UserSearch(APIView):
    def get(self,request):
        try:
            user, token = TokenAuthentication().authenticate(request)
        except AuthenticationFailed as e:
            return Response({"message": "Invalid auth token"}, status=status.HTTP_401_UNAUTHORIZED)

        form = request.GET
        # =========================  When Searched With Phone Number ======================================#
        if 'mobile' in form.keys():
            try:
                userSearched = User.objects.get(mobile=form.get('mobile'))

                #============ Check if user who is searching is in the person’s contact list =====================#
                try:
                    p = PersonalContact.objects.get(user=userSearched,mobile=user.mobile)
                    return Response({
                        'Name': userSearched.name,
                        "Mobile": userSearched.mobile,
                        "Email":userSearched.email,
                    })
                except PersonalContact.DoesNotExist:
                    return Response({
                        'Name':userSearched.name,
                        "Mobile":userSearched.mobile,
                    })

            except User.DoesNotExist:
                data = {}
                spammed = SpammedNumbers.objects.filter(mobile=form.get('mobile')).first()
                if not spammed:
                    data['isspamed'] = False
                    data["spammed_by"] = 0
                else:
                    data['isspamed'] = True
                    data['spammed_by'] = spammed.spammed_by.all().count()


                personal = PersonalContact.objects.filter(mobile=form.get('mobile'))
                if not personal:
                    data['inPersonal'] = False
                    data['related_names'] = []
                else:
                    data['inPersonal'] = True
                    data['related_names'] = []
                    for p in personal:
                        data['related_names'].append(p.name)

                return Response(data)

        # ======================================= Search By Name ================================================ #
        elif 'name' in form.keys():
            users = User.objects.filter(name__icontains=form.get('name'))
            if users:
                data = []
                for user in users:
                    data.append({
                        'Name':user.name,
                        'Mobile':user.mobile
                    })
                return Response(data)
            else:
                data = {}
                spammed = SpammedNumbers.objects.filter(spammed_name__icontains=form.get('name'))
                if not spammed:
                    data['isspamed'] = False
                    data['isspamed'] = 0
                else:
                    data['isspamed'] = True
                    data['spammed_by'] = spammed.spammed_by.all().count()

                personal = PersonalContact.objects.filter(name__icontains=form.get('mobile'))
                if not personal:
                    data['inPersonal'] = False
                    data['related_names'] = []
                else:
                    data['inPersonal'] = True
                    data['related_names'] = []
                    for p in personal:
                        data['related_names'].append(p.name)

                return Response(data)




