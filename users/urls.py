from django.urls import path,include
from .views import (SignupUser, Login,
                    MarkSpam,AddPersonalContacts,
                    UserSearch)
app_name = 'users'

urlpatterns = [
    path('api/signup/',SignupUser.as_view()),
    path('api/login/',Login.as_view()),
    path('api/mark-spam/',MarkSpam.as_view()),
    path('api/add-personal-contact/',AddPersonalContacts.as_view()),
    path('api/search/',UserSearch.as_view()),
]