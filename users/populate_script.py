from .models import *
from rest_framework.authtoken.models import Token
def adddata():
    from faker import Faker
    fake = Faker()
    start_number = 9811375462
    for i in range(100):
        fake_username = fake.name()
        fake_name = fake.name()
        fake_email = fake.email()
        password = fake.password()

        user = User.objects.create(username=fake_username,name=fake_name,email=fake_email,mobile=start_number)
        user.set_password(password)
        user.save()
        token = Token.objects.create(user=user)
        start_number -=1