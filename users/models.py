# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.validators import MaxValueValidator


# Create your models here.

class User(AbstractUser):
    mobile = models.BigIntegerField(unique=True,default=0000000000,validators=[MaxValueValidator(9999999999)])
    name = models.CharField(max_length=200,null=True,blank=True)
    email = models.EmailField(max_length=400,null=True,blank=True,unique=True)


    def __str__(self):
        return "Username : {} and Mobile : {}".format(self.username,self.mobile)


    def get_all_personal_contact(self):
        personalContact = PersonalContact.objects.filter(user=self)
        return personalContact
    def spanned_by_user(self):
        spammed = SpammedNumbers.objects.filter(spanned_by=self)
        return spammed


class SpammedNumbers(models.Model):
    mobile     = models.BigIntegerField(null=False,blank=False,validators=[MaxValueValidator(9999999999)])
    spammed_by = models.ManyToManyField(User,related_name='user_spammed')
    spammed_name = models.CharField(max_length=200,null=True,blank=True)

    def __str__(self):
        return str(self.mobile)

    def get_spammed_numbers_user(self):
        arr = []
        for user in self.spammed_by.all():
            arr.append(user.mobile)
        return arr




class PersonalContact(models.Model):
    mobile = models.BigIntegerField(null=False,blank=False,validators=[MaxValueValidator(9999999999)])
    name   = models.CharField(null=True,blank=True,max_length=200)
    user = models.ForeignKey(User,related_name='user_contacts',
                             validators=[MaxValueValidator(9999999999)],on_delete=models.CASCADE)


    def __str__(self):
        return str(self.mobile)

