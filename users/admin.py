# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import *

# Register your models here.
@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    model = User
    list_display = ['username', 'mobile', 'email']
    list_filter = ['username', 'mobile']
    search_fields = list_filter

@admin.register(SpammedNumbers)
class SpammedNumbersAdmin(admin.ModelAdmin):
    model = SpammedNumbers
    list_display = ['mobile']
    list_filter = ['mobile']
    search_fields = list_filter

@admin.register(PersonalContact)
class PersonalContactAdmin(admin.ModelAdmin):
    model = PersonalContact
    list_display = ['mobile','user']
    list_filter = ['mobile','user']
    search_fields = list_filter